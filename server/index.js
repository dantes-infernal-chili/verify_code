const express = require('express')
const { graphqlHTTP } = require('express-graphql');
const cors = require('cors');
const schema = require('./schema')
const { v4: uuidv4 } = require('uuid')

const PORT = 5000
const users = []

const app = express()
app.use(cors())

const createUser = input => {
  const id = Date.now()
  const verCode = uuidv4()
  return {
    id,
    verCode,
    ...input
  }
}

const root = {
  getAllUsers: () => users,
  createUser: ({ input }) => {
    const user = createUser(input)
    users.push(user)
    return user
  }
}

app.use('/graphql', graphqlHTTP({
  graphiql: true,
  schema,
  rootValue: root
}))

app.listen(PORT, () => {
  console.log('Server running on port ' + PORT)
})
const { buildSchema } = require('graphql');

const schema = buildSchema(`
  type User {
    id: ID
    email: String
    verCode: String
  }

  input UserInput {
    id: ID
    email: String!
    verCode: String
  }

  type Query {
    getAllUsers: [User]
  }

  type Mutation {
    createUser(input: UserInput): User
  }
`)

module.exports = schema
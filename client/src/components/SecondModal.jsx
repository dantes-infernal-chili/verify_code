import React from 'react'
import { Input, Form } from './DOMElements.js'
import { useQuery} from '@apollo/client';
import { GET_ALL_USERS } from '../query/user.js';

export default function SecondModal() {
  const [val, setVal] = React.useState('')
  const [markFlag, setMarkFlag] = React.useState(false)
  const {data, loading} = useQuery(GET_ALL_USERS) 
  let verCode = ''
  if (!loading) {
    verCode = data.getAllUsers[data.getAllUsers.length - 1].verCode
  }

  React.useEffect(() => {
    console.log(verCode)
  }, [verCode])

  const handleChange = (value) => {
    setVal(value)
    setMarkFlag(validateCode(verCode, value))
  }

  const validateCode = (code, currentValue) => code === currentValue

  return (
    <div>
      <Form>
        <Input style={markFlag === true ? {color: '#00FF00'} : {color: '#FF0000'}} value={val} type="text" placeholder="Enter ur code here..." onChange={event => handleChange(event.target.value)}/>
      </Form>
    </div>
  )
}

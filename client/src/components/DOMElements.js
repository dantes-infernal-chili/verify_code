import styled from "styled-components"

export const Input = styled.input`
    font-size: 18px;
    padding: 10px;
    display: block;
    margin-bottom: 15px;
    background: papayawhip;
    border: none;
    border-radius: 3px;
    ::placeholder {
      color: palevioletred;
    }
  `

export const Form = styled.form`
    width: 400px;
    height: 200px;
    border: none;
    box-shadow: 0 0 10px rgba(0,0,0,0.5);
    background-color: #FAFAFA;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  `

export const SubmitInput = styled.input`
  font-family: monospace;
  display: inline-block;
  color: palevioletred;
  font-size: 18px;
  padding: 10px 20px;
  border: 2px solid palevioletred;
  border-radius: 3px;
  display: block;
  text-decoration: none;
  cursor: pointer;
  :hover {
      box-shadow: 0 0 10px rgba(0,0,0,0.5);
    }
`

export const Success = styled.div`
  display: block;
  width: 500px;
  height: 50px;
  font-size: 25px;
  font-weight: bold;
  position: absolute;
  line-height: 50px;
  text-align: center;
  color: #FFFFFF;
  background-color: #00FF00;
  left: 50%;
  top: 5%;
  transform: translateX(-50%);
  border: 3px solid #00ff00;
  border-radius: 5px;
`
import React from 'react';
import FirstModal from './components/FirstModal';
import SecondModal from './components/SecondModal';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"
import createHistory from 'history/createBrowserHistory'

export const history = createHistory()

function App() {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact>
          <FirstModal />
        </Route>
        <Route path="/validate">
          <SecondModal />
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
